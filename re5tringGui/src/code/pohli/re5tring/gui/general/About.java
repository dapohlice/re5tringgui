package code.pohli.re5tring.gui.general;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class About extends JPanel {
	
	public About(){
		GridLayout gl = new GridLayout(5, 1);
		this.setLayout(gl);
		
		JPanel pheader = new JPanel();
		
		pheader.setLayout(new FlowLayout(FlowLayout.CENTER));
		
		JLabel lblHeader = new JLabel("re5tring");
		pheader.add(lblHeader);
		
		this.add(pheader);
		
		
		JPanel picon = new JPanel();
		
		picon.setLayout(new FlowLayout(FlowLayout.CENTER));
		JLabel lblIcon = new JLabel("Icons by");
		
		JLabel lblIcon8 = new JLabel("icons8.com");
		
		picon.add(lblIcon);
		picon.add(lblIcon8);
		
		this.add(picon);
		
		
		
	}
	
	
	
}
