package code.pohli.re5tring.gui.general;

import javax.swing.JFrame;

public class AboutFrame extends JFrame {
	public AboutFrame(){
		this.setTitle("About");
		
		
		this.add(new About());
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		new AboutFrame();
	}
}
