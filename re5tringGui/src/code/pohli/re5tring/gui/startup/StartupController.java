package code.pohli.re5tring.gui.startup;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;

import code.pohli.gui.swing.style.StyleColor;

public class StartupController implements ActionListener {

	private StartupModel model;
	private StartupView view;
	
	public StartupController() {
		this.model = new StartupModel();
		this.view = new StartupView(this);
		
		this.model.addObserver(view);
	}
	
	
	
	public StartupModel getModel() {
		return model;
	}



	public StartupView getView() {
		return view;
	}



	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		switch(arg0.getActionCommand())
		{
			case "source":
				String src = this.view.getSource();
				this.model.setSource(openDirectoryDialog("Source", src));				
				
				break;
			case "placeholder":
				String pl = this.view.getPlaceholder();
				this.model.setPlaceholder(openDirectoryDialog("Placeholder", pl));				
				
				break;
			case "destination":
				String dest = this.view.getDestination();
				this.model.setDestination(openDirectoryDialog("Destination", dest));
				break;
			case "workingdir":
				String work = this.view.getWorkingDir();
				this.model.setWorkingdir(openDirectoryDialog("Working", work));
				
				if(!this.model.isWorkingDirEmpty())
				{
					if(!this.model.isWorkingDirAlreadyExists())
					{
						this.view.setStatus("Directory is not empty");
					}else{
						this.view.setStatus("re5tring Project detected");
					}
				}else{
					this.view.setStatus("create new re5tring Project");
				}
				
				break;
			case "create":
				
				this.model.setWorkingdir(this.view.getWorkingDir());
				this.model.setDestination(this.view.getDestination());
				
				this.model.setError(this.view.getError());
				this.model.setLinebreak(this.view.getLineBreak());
				this.model.setPlaceholder(this.view.getPlaceholder());
				this.model.setSource(this.view.getSource());
								
				this.model.create();
				
				break;
		}
	}

	private String openDirectoryDialog(String txt, String path){
		JFileChooser chooser = new JFileChooser(path);
		chooser.setBackground(StyleColor.Color.getLightA());
		
		chooser.setFileHidingEnabled(true);
		
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		chooser.setMultiSelectionEnabled(false);
		
		chooser.setDialogType(JFileChooser.OPEN_DIALOG);
		
		int ret = chooser.showDialog(null, "Choose "+txt + " Directory");
		
		if(ret == JFileChooser.APPROVE_OPTION)
		{
			return chooser.getSelectedFile().getAbsolutePath();
		}
		
		return path;
	}
	
	
	public void chkRootChanged(boolean isSelected){
		this.model.setUseRoot(isSelected);
	}
	
	public void chkDefaultChanged(boolean isSelected){
		this.model.setUseDefault(isSelected);
	}
	
	public String getError() {
		return this.model.getError();
	}

	public String getLinebreak() {
		return this.model.getLinebreak();
	}

	public String getPlaceholder() {
		return this.model.getPlaceholder();
	}

	public String getSource() {
		return this.model.getSource();
	}

	public String getDestination() {
		return this.model.getDestination();
	}

	public String getWorkingdir() {
		return this.model.getWorkingdir();
	}
	
	public boolean isRoot(){
		return this.model.isUseRoot();
	}
	
	public boolean isDefault(){
		return this.model.isUseDefault();
	}
}
