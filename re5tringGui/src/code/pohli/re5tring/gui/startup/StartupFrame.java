package code.pohli.re5tring.gui.startup;

import javax.swing.JFrame;

import code.pohli.utils.console.Console;

public class StartupFrame extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2787574505041891369L;

	public StartupFrame(){
		
		this.setTitle("Start re5tring Project");
		
		
		this.add(new StartupPanel());
		
		
		
		this.setSize(600,400);	
		
		this.setVisible(true);
		
	}
	
	
	public static void main(String[] args) {
		Console.console.setDebug(true);
		
		new StartupFrame();
	}
}
