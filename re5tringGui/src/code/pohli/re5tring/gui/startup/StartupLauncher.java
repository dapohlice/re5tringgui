package code.pohli.re5tring.gui.startup;

import javax.swing.JFrame;

import code.pohli.utils.console.Console;

public class StartupLauncher {
	public static void main(String[] args) {
		
		JFrame f = new JFrame();
		
		Console.console.setDebug(true);
		
		f.setTitle("Startup");
		f.setVisible(true);
		
		StartupController controller = new StartupController();
		
		f.add(controller.getView());
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.pack();
	}
}
