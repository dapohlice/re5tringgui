package code.pohli.re5tring.gui.startup;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Observable;
import java.util.stream.Stream;

import code.pohli.re5tring.placeholder.variables.PVarList;
import code.pohli.re5tring.settings.Settings;
import code.pohli.utils.console.Console;

public class StartupModel extends Observable {
	
	private static final String DEFAULT_SOURCE = "source";
	private static final String DEFAULT_DESTINATION = "desination";
	private static final String DEFAULT_PLACEHOLDER = "placeholder";
	
	
	private String error;
	private String linebreak;
	
	private String placeholder;
	private String source;
	private String destination;
	private String workingdir;
	
	private boolean useRoot;
	private boolean useDefault;
	
	private boolean workingDirEmpty;
	private boolean workingDirAlreadyExists;
	
	public StartupModel(){
		error = "";
		linebreak  = "";
		placeholder = "";
		source = "";
		destination = "";
		workingdir = "";
		useRoot = false;
		useDefault = false;
		workingDirEmpty = true;
		workingDirAlreadyExists = false;
	}

	
	public boolean create(){
		Settings settings = new Settings();
		settings.setErrorPlaceholder(this.error);
		settings.setLineBreak(this.linebreak);
		
		
		Path pwork = Paths.get(workingdir).toAbsolutePath();
		
		if(!Files.exists(pwork)){
			try {
				Files.createDirectory(pwork);
				settings.setWorkingDir(pwork.toString());
			} catch (IOException e) {
				Console.error("Failed create Working Directory "+workingdir);
				return false;
			}
		}
		
		Path psource;
		Path pplace;
		Path pdest;
		
		
		if(useRoot)
		{
			psource = pwork.resolve(this.source);
			pplace = pwork.resolve(this.placeholder);
			pdest = pwork.resolve(this.destination);
			
			Console.debug("Source "+psource.toString());
			Console.debug("Placeholder "+pplace.toString());
			Console.debug("Destination "+pdest.toString());
		}else{
			psource = Paths.get(this.source).toAbsolutePath();
			pplace = Paths.get(this.placeholder).toAbsolutePath();
			pdest = Paths.get(this.destination).toAbsolutePath();
		}
		
		if(!Files.exists(psource))
		{
			try {
				Files.createDirectories(psource);
			} catch (IOException e) {
				Console.error("Failed creating Source Directory "+psource.toString());
				
				return false;
			}
		}
		
		if(!Files.exists(pplace))
		{
			try {
				Files.createDirectories(pplace);
			} catch (IOException e) {
				Console.error("Failed creating Placeholder Directory "+pplace.toString());
				return false;
			}
		}
		
		if(!Files.exists(pdest))
		{
			try {
				Files.createDirectories(pdest);
			} catch (IOException e) {
				Console.error("Failed creating Destination Directory "+pdest.toString());
				return false;
			}
		}
		
		settings.setSourceDirectory(this.source);
		settings.setDestinationDirectory(this.destination);
		settings.setPlaceholderDirectory(this.placeholder);
		
		settings.save(pwork.toString());
		
		if(!PVarList.fileExists(pwork.toString()))
		{
			PVarList varlist = new PVarList();
			varlist.save(pwork.toString());
		}
		setChanged();
		notifyObservers(this);
		return true;
	}
	
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;

	}

	public String getLinebreak() {
		return linebreak;
	}

	public void setLinebreak(String linebreak) {
		this.linebreak = linebreak;

	}

	public String getPlaceholder() {
		return placeholder;
	}

	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;

	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;

	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;

	}

	public String getWorkingdir() {
		return workingdir;
	}

	public void setWorkingdir(String workingdir) {
		this.workingdir = workingdir;
		
		
		if(Settings.settingsExists(workingdir))
		{
			Settings s = Settings.loadSettings(workingdir);
			destination = s.getDestinationDirectory();
			source = s.getSourceDirectory();
			placeholder = s.getPlaceholderDirectory();
			
			useRoot = s.isUseWorkingDir();
			
			linebreak = s.getLineBreak();
			error = s.getErrorPlaceholder();
			
			workingDirAlreadyExists = true;
		}else{
			workingDirAlreadyExists = false;
		}
		
		Stream<Path> s;
		try {
			s = Files.list(Paths.get(workingdir));
			if(s.findFirst() != null){
				workingDirEmpty =false;
			}else{
				workingDirEmpty =true;
			}
		} catch (IOException e) {
			workingDirEmpty =true;
		}

		
		setChanged();
		notifyObservers(this);
	}

	public boolean isUseRoot() {
		return useRoot;
	}

	public void setUseRoot(boolean useRoot) {
		this.useRoot = useRoot;

	}

	public boolean isUseDefault() {
		return useDefault;
	}

	public void setUseDefault(boolean useDefault) {
		this.useDefault = useDefault;
		
		if(useDefault)
		{
			this.setDestination(DEFAULT_DESTINATION);
			this.setPlaceholder(DEFAULT_PLACEHOLDER);
			this.setSource(DEFAULT_SOURCE);	
			this.useRoot = true;
			setChanged();
			notifyObservers(this);
		}
		

	}


	public boolean isWorkingDirEmpty() {
		return workingDirEmpty;
	}


	public boolean isWorkingDirAlreadyExists() {
		return workingDirAlreadyExists;
	}
	
	
	
	
}
