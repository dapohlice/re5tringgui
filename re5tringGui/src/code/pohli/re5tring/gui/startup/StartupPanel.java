package code.pohli.re5tring.gui.startup;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.LayoutManager;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.Border;

import code.pohli.gui.swing.components.StyleButton;
import code.pohli.gui.swing.components.StyleImageButton;
import code.pohli.gui.swing.style.StyleColor;
import code.pohli.gui.swing.style.StyleImages;
import code.pohli.re5tring.placeholder.variables.PVarList;
import code.pohli.re5tring.settings.Settings;
import code.pohli.utils.console.Console;

public class StartupPanel extends JPanel{

	private static final Color mainColor = StyleColor.Color.getDarkA();
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3867819584605588700L;

	private JTextField txtLinebreak;
	private JTextField txtError;
	private JTextField txtPlaceholder;
	private JTextField txtSource;
	private JTextField txtDestination;
	private JTextField txtWorking;
	private JCheckBox chkRoot;
	
	private List<JButton> btnList;
	private JPanel pdetail;
	
	public StartupPanel(){
		
		JPanel main = new JPanel();
		
		btnList = new ArrayList<JButton>(3);
		
		LayoutManager gl = new BoxLayout( main, BoxLayout.Y_AXIS);
		
		 main.setLayout(gl);
		
		
		
		txtWorking = createInputPanel( main,"Working Directory (Settings File)",null,true);
		
		JPanel poptions = new JPanel();
		FlowLayout fl = new FlowLayout(FlowLayout.RIGHT);


		poptions.setLayout(fl);
		
		

		
		JCheckBox chkDefault = new JCheckBox();
		chkDefault.setText("default");
		poptions.add(chkDefault);
		
		chkRoot = new JCheckBox();
		chkRoot.setText("working directory is root");
		poptions.add(chkRoot);
		
		main.add(poptions);
		
		JSeparator sep = new JSeparator(JSeparator.HORIZONTAL);
		sep.setForeground(mainColor);
		
		main.add(sep);
		
		pdetail = new JPanel();
		BoxLayout bl = new BoxLayout(pdetail, BoxLayout.Y_AXIS);
		pdetail.setLayout(bl);
		
		
		
		txtSource = createInputPanel(pdetail,"Source Directory",btnList,true);
		txtPlaceholder = createInputPanel(pdetail,"Placeholder Directory",btnList,true);
		txtDestination = createInputPanel(pdetail,"Destination Directory",btnList,true);
		
		main.add(pdetail);
		
		JSeparator sepB = new JSeparator(JSeparator.HORIZONTAL);
		sepB.setForeground(mainColor);

		main.add(sepB);
		
		JPanel psettings = new JPanel();
		BoxLayout blset = new BoxLayout(psettings, BoxLayout.Y_AXIS);
		psettings.setLayout(blset);
		
		txtLinebreak = createInputPanel(psettings, "Line Break", null,false);
		txtError = createInputPanel(psettings, "Error Placeholder", null,false);
		
		main.add(psettings);
		
		chkDefault.addItemListener(evnet->{
			Console.debug("StartupPanel chkDefault change");
			
			
			if(chkDefault.isSelected())
			{
				Console.debug("chkDefault selected");
				enablePanel(pdetail,false);
				
				chkRoot.setSelected(true);
				chkRoot.setEnabled(false);
				txtSource.setText("source");
				txtDestination.setText("destination");
				txtPlaceholder.setText("placeholder");
				
			}else{
				Console.debug("chkDefault not selected");
				enablePanel(pdetail,true);
				chkRoot.setEnabled(true);
				
			}
		}
		);
		
		chkRoot.addItemListener(event->{
			Console.debug("StartupPanel chkRoot change");
			if(chkRoot.isSelected())
			{
				Console.debug("chkRoot selected");
				for(JButton b: btnList){
					b.setEnabled(false);
				}
				
			}else{
				Console.debug("chkRoot not selected");
				if(!chkDefault.isSelected())
				{
					for(JButton b: btnList){
						b.setEnabled(true);
					}
					
				}
				
			}
		});
		Border mainborder = BorderFactory.createLineBorder(mainColor);//BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);

		main.setBorder(mainborder);
		
		LayoutManager blthis = new BoxLayout( this, BoxLayout.Y_AXIS);
		this.setLayout(blthis);
		this.add(main);
		
		JPanel pbtn = new JPanel();
		FlowLayout flbtn = new FlowLayout(FlowLayout.RIGHT);
		pbtn.setLayout(flbtn);
		
		JButton btncreate = new StyleButton("Create");
		pbtn.add(btncreate);
		
		btncreate.addActionListener(event-> create());
		
		this.add(pbtn);
		
		
	}
	
	private void create(){
		Settings settings = new Settings();
		settings.setErrorPlaceholder(txtError.getText());
		settings.setLineBreak(txtLinebreak.getText());
		
		String working = txtWorking.getText();
		
		Path pwork = Paths.get(working).toAbsolutePath();
		
		if(!Files.exists(pwork)){
			try {
				Files.createDirectory(pwork);
				settings.setWorkingDir(pwork.toString());
			} catch (IOException e) {
				Console.error("Failed create Working Directory "+working);
				return;
			}
		}
		
		Path psource;
		Path pplace;
		Path pdest;
		
		String ssource = txtSource.getText();
		String splace = txtPlaceholder.getText();
		String sdest = txtDestination.getText();
		
		if(chkRoot.isSelected())
		{
			psource = pwork.resolve(ssource);
			pplace = pwork.resolve(splace);
			pdest = pwork.resolve(sdest);
			
			Console.debug("Source "+psource.toString());
			Console.debug("Placeholder "+pplace.toString());
			Console.debug("Destination "+pdest.toString());
		}else{
			psource = Paths.get(ssource).toAbsolutePath();
			pplace = Paths.get(splace).toAbsolutePath();
			pdest = Paths.get(sdest).toAbsolutePath();
		}
		
		if(!Files.exists(psource))
		{
			try {
				Files.createDirectories(psource);
			} catch (IOException e) {
				Console.error("Failed creating Source Directory "+psource.toString());
				
				return;
			}
		}
		
		if(!Files.exists(pplace))
		{
			try {
				Files.createDirectories(pplace);
			} catch (IOException e) {
				Console.error("Failed creating Placeholder Directory "+pplace.toString());
				return;
			}
		}
		
		if(!Files.exists(pdest))
		{
			try {
				Files.createDirectories(pdest);
			} catch (IOException e) {
				Console.error("Failed creating Destination Directory "+pdest.toString());
				return;
			}
		}
		
		settings.setSourceDirectory(ssource);
		settings.setDestinationDirectory(sdest);
		settings.setPlaceholderDirectory(splace);
		
		PVarList varlist = new PVarList();
		varlist.save(pwork.toString());
		
		
		
	}
	
	
	public JTextField createInputPanel(JPanel mainPanel, String txtLabel, List<JButton> btnList, boolean addButton){

		JPanel panel = new JPanel();
		FlowLayout fl = new FlowLayout(FlowLayout.RIGHT);


		panel.setLayout(fl);
		
		JLabel lbl = new JLabel(txtLabel);
		panel.add(lbl);
		
		JTextField txt = new JTextField(20);
		
		panel.add(txt);
		if(addButton)
		{
			Image img = StyleImages.loadImage(Paths.get("resources", "images","folder.png"));
			
			JButton btn = new StyleImageButton("open",img,32);
			
			if(btnList != null)
			{
				btnList.add(btn);
			}
			
			btn.addActionListener(event->{
				
				
				
				
				
			});
			
			panel.add(btn);
		}
		mainPanel.add(panel);
		
		return txt;
	}
	
	private void enablePanel(JPanel panel, boolean enable )
	{
		panel.setEnabled(enable);
		for(Component c : panel.getComponents()){
			c.setEnabled(enable);
			if(c instanceof JPanel)
			{
				enablePanel((JPanel)c, enable);
			}
		}
	}
	
}
