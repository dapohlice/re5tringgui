package code.pohli.re5tring.gui.startup;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.LayoutManager;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.Border;

import code.pohli.gui.swing.components.StyleButton;
import code.pohli.gui.swing.components.StyleImageButton;
import code.pohli.gui.swing.style.StyleColor;
import code.pohli.gui.swing.style.StyleImages;

public class StartupView extends JPanel implements Observer{
	private static final Color mainColor = StyleColor.Color.getDarkA();
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -251923917908624132L;

	private JTextField txtLinebreak;
	private JTextField txtError;
	private JTextField txtPlaceholder;
	private JTextField txtSource;
	private JTextField txtDestination;
	private JTextField txtWorking;
	private JCheckBox chkRoot;
	private JCheckBox chkDefault;
	
	private JLabel lblStatusWorkingDir;
	
	private JPanel pdetail;

	StartupController controller;
	
	private List<JButton> btnList;
	
	public StartupView(StartupController controller){
		
		this.controller = controller;
		
		JPanel main = new JPanel();
		
		btnList = new ArrayList<JButton>(3);
		
		LayoutManager gl = new BoxLayout( main, BoxLayout.Y_AXIS);
		
		main.setLayout(gl);
		
		txtWorking = createInputPanel( main,"Working Directory (Settings File)","workingdir" ,null,true,controller);
		
		
		JPanel psecwork = new JPanel();
		psecwork.setLayout(new BorderLayout());
		JPanel poptions = new JPanel();
		FlowLayout fl = new FlowLayout(FlowLayout.RIGHT);
		
		poptions.setLayout(fl);
		
		
		lblStatusWorkingDir = new JLabel();
		lblStatusWorkingDir.setBorder(BorderFactory.createEmptyBorder(0,20,0,0));
		psecwork.add(lblStatusWorkingDir,BorderLayout.WEST);
		
		chkDefault = new JCheckBox();
		chkDefault.addItemListener(event->{
			controller.chkDefaultChanged(isDefaultSelected());
		});
		chkDefault.setText("default");
		poptions.add(chkDefault);
		
		chkRoot = new JCheckBox();
		chkRoot.addItemListener(event->{
			controller.chkRootChanged(isRootSelected());
		});
		chkRoot.setText("working directory is root");
		poptions.add(chkRoot);
		
		psecwork.add(poptions,BorderLayout.EAST);
		main.add(psecwork);
		
		JSeparator sep = new JSeparator(JSeparator.HORIZONTAL);
		sep.setForeground(mainColor);
		
		main.add(sep);
		
		pdetail = new JPanel();
		BoxLayout bl = new BoxLayout(pdetail, BoxLayout.Y_AXIS);
		pdetail.setLayout(bl);
		
		
		txtSource = createInputPanel(pdetail,"Source Directory","source",btnList,true,controller);
		txtPlaceholder = createInputPanel(pdetail,"Placeholder Directory","placeholder",btnList,true,controller);
		txtDestination = createInputPanel(pdetail,"Destination Directory","destination",btnList,true,controller);
		
		main.add(pdetail);
		
		JSeparator sepB = new JSeparator(JSeparator.HORIZONTAL);
		sepB.setForeground(mainColor);

		main.add(sepB);
		
		JPanel psettings = new JPanel();
		BoxLayout blset = new BoxLayout(psettings, BoxLayout.Y_AXIS);
		psettings.setLayout(blset);
		
		txtLinebreak = createInputPanel(psettings, "Line Break","", null,false,controller);
		txtError = createInputPanel(psettings, "Error Placeholder","", null,false,controller);
		
		main.add(psettings);
		
		Border mainborder = BorderFactory.createLineBorder(mainColor);//BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);

		main.setBorder(mainborder);
		
		LayoutManager blthis = new BoxLayout( this, BoxLayout.Y_AXIS);
		this.setLayout(blthis);
		this.add(main);
		
		JPanel pbtn = new JPanel();
		FlowLayout flbtn = new FlowLayout(FlowLayout.RIGHT);
		pbtn.setLayout(flbtn);
		
		JButton btncreate = new StyleButton("Create");
		pbtn.add(btncreate);
		
		btncreate.addActionListener(controller);
		btncreate.setActionCommand("create");
		
		
		this.add(pbtn);
	}
	
	public JTextField createInputPanel(
			JPanel mainPanel, 
			String txtLabel, 
			String command, 
			List<JButton> btnList, 
			boolean addButton,
			StartupController controller){

		JPanel panel = new JPanel();
		FlowLayout fl = new FlowLayout(FlowLayout.RIGHT);


		panel.setLayout(fl);
		
		JLabel lbl = new JLabel(txtLabel);
		panel.add(lbl);
		
		JTextField txt = new JTextField(20);
		
		panel.add(txt);
		if(addButton)
		{
			Image img = StyleImages.loadImage(Paths.get("resources", "images","folder.png"));
			
			JButton btn = new StyleImageButton("open",img,32);
			
			if(btnList != null)
			{
				btnList.add(btn);
			}
			
			btn.addActionListener(controller);
			btn.setActionCommand(command);
			
			
			panel.add(btn);
		}
		mainPanel.add(panel);
		
		return txt;
	}
	
	@Override
	public void update(Observable ob, Object obj) {
		
		txtDestination.setText(this.controller.getDestination());
		txtSource.setText(this.controller.getSource());
		txtPlaceholder.setText(this.controller.getPlaceholder());
		txtLinebreak.setText(this.controller.getLinebreak());
		txtError.setText(this.controller.getError());
		txtWorking.setText(this.controller.getWorkingdir());
		
		chkRoot.setSelected(this.controller.isRoot());
		chkDefault.setSelected(this.controller.isDefault());
		
		if(this.controller.isDefault())
		{
			enablePanel(pdetail, false);
			chkRoot.setEnabled(false);
		}else{
			chkRoot.setEnabled(true);
			enablePanel(pdetail, true);
		}
		
		if(this.controller.isRoot())
		{
			for(JButton b: btnList){
				b.setEnabled(false);
			}
		}else{
			if(!this.controller.isDefault())
			{
				for(JButton b: btnList){
					b.setEnabled(true);
				}
			}
		}
		
	}
	
	public void setStatus(String status)
	{
		lblStatusWorkingDir.setText(status);
	}
	
	private void enablePanel(JPanel panel, boolean enable )
	{
		panel.setEnabled(enable);
		for(Component c : panel.getComponents()){
			c.setEnabled(enable);
			if(c instanceof JPanel)
			{
				enablePanel((JPanel)c, enable);
			}
		}
	}
	
	public String getLineBreak(){
		return txtLinebreak.getText();
	}
	
	public String getError(){
		return txtError.getText();
	}
	
	public String getDestination(){
		return txtDestination.getText();
	}
	
	public String getPlaceholder(){
		return txtPlaceholder.getText();
	}
	
	public String getSource(){
		
		String s = txtSource.getText();
		return s;
	}
	
	public String getWorkingDir(){
		return txtWorking.getText();
	}
	
	public boolean isRootSelected(){
		return chkRoot.isSelected();
	}
	
	public boolean isDefaultSelected(){
		return chkDefault.isSelected();
	}
	
	

}
