package code.pohli.re5tring.gui.variables;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import code.pohli.re5tring.placeholder.variables.PVarList;
import code.pohli.re5tring.placeholder.variables.PVariable;
import code.pohli.utils.console.Console;

public class VariableTableModel extends AbstractTableModel implements TableModel{

	public static String COL_NAME = "Name";
	public static String COL_VALUE= "Value";
	
	
	
	private class Data{
		public String name;
		public String value;
		
		public Data() {
			this.name = "";
			this.value = "";
		}
		
		public Data(String name, String value)
		{
			this.name = name;
			this.value = value;
		}
	}
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8861248790053575914L;
	private PVarList varList;
	private List<Data> list;
	
	private boolean changed;
	
	
	public VariableTableModel(PVarList list)
	{
		this.varList = list;
		
		this.init();
	}
	
	
	private void init() {
		this.list = new ArrayList<Data>();
		for(PVariable var :this.varList.getList()) {
			this.list.add(new Data(var.getFullName(),var.getValue()));
		}
		this.changed = false;
	}
	
	
	public PVarList getVarList() {
		PVarList vlist = new PVarList();
		for(Data d: this.list)
		{
			PVariable v = new PVariable();
			v.setFullName(d.name);
			v.setVar(d.value);
			vlist.addVariable(v);
		}
		
		
		
		return vlist;
	}
	
	
	@Override
	public int getColumnCount() {
		return 2;
	}

	@Override
	public int getRowCount() {
		return this.list.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Data d = this.list.get(rowIndex);
		if(d == null)
			return null;
		
		if(columnIndex == 0)
		{
			return d.name;
		}else {
			return d.value;
		}
	}



	@Override
	public int findColumn(String columnName) {
		// TODO Auto-generated method stub
		return super.findColumn(columnName);
	}



	@Override
	public Class<?> getColumnClass(int columnIndex) {
		// TODO Auto-generated method stub
		return String.class;
	}



	@Override
	public String getColumnName(int column) {
		if(column == 0)
		{
			return COL_NAME;
		}else {
			return COL_VALUE;
		}
	}



	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}



	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		Console.debug("Set Value "+aValue);
		
		if(aValue instanceof String)
		{
			String v = (String)aValue;
			Data d = this.list.get(rowIndex);
			
			
			if(columnIndex == 0)
			{
				d.name = v;
			}else {
				d.value = v;
			}
			
			this.changed = true;
		}
	}


	public boolean isChanged() {
		return this.changed;
	}

	public void setUnchanged() {
		this.changed = false;
	}
	
	public void reset() {
		
		this.init();
		
		this.changed = false;
		
		this.fireTableDataChanged();
	}
	
	public void createVariable() {
		Data d = new Data();
		this.list.add(d);
		this.fireTableRowsInserted(this.list.size()-1, this.list.size()-1);
		
		
		
		this.changed = true;
	}
	
	public void deleteRows(int[] rows)
	{
		List<Data> toRemove = new ArrayList<Data>(rows.length);
		for(int r:rows)
		{
			toRemove.add(this.list.get(r));
			
		}
		
		for(Data d : toRemove)
		{
			this.list.remove(d);
		}
		

		this.fireTableDataChanged();
		this.changed = true;
	}

	
}
