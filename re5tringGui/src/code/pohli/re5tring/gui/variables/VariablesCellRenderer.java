package code.pohli.re5tring.gui.variables;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class VariablesCellRenderer extends DefaultTableCellRenderer {

	public static final Color C_FOCUS = new Color(62, 165, 193);// new Color(178, 178, 178);
	public static final Color C_SELECTED = new Color(94, 94, 94);

	/**
	 * 
	 */
	private static final long serialVersionUID = -2340384196461305058L;
	
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {

		Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		
		if(hasFocus)
		{
			c.setBackground(C_FOCUS);
			
		}else{
			if(isSelected)
			{
				c.setBackground(C_SELECTED);
			}else{
				c.setBackground(Color.WHITE);
			}
			
			
		}
		
		return c;
	}
}
