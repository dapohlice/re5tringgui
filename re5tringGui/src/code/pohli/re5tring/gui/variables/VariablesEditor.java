package code.pohli.re5tring.gui.variables;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableColumn;

import code.pohli.re5tring.placeholder.variables.PVarList;
import code.pohli.re5tring.settings.General;
import code.pohli.utils.console.Console;

public class VariablesEditor extends JPanel implements KeyListener{

	private JTable table;
	private VariableTableModel tableModel;
	
	
	public VariablesEditor() {
		LayoutManager fl = new BorderLayout(10,10);
		this.setLayout(fl);
		
		this.table = new JTable();
		
		
		this.table.setFillsViewportHeight(true);		
		this.table.setDefaultRenderer(String.class,new VariablesCellRenderer());
		
		this.table.setAutoCreateRowSorter(true);
		
		
		JScrollPane scroll = new JScrollPane(table);
		this.add(scroll);
		table.addKeyListener(this);
		this.addKeyListener(this);
		
	}
	
	
	
	public void setVarList(PVarList list) {
		this.tableModel = new VariableTableModel(list);
		this.table.setModel(this.tableModel);
		
		
		TableColumn colName = table.getColumnModel().getColumn(0);
		colName.setMinWidth(50);
		colName.setPreferredWidth(100);

		
		TableColumn colValue = table.getColumnModel().getColumn(1);
		colValue.setMinWidth(50);

	}
	
	public PVarList getVarList() {
		return this.tableModel.getVarList();
	}

	public void reset() {
		this.tableModel.reset();
	}
	
	public boolean save() {
		
		PVarList vlist = this.tableModel.getVarList();
		
		boolean ret = vlist.save(General.getSettings().getWorkingDir());
		
		if(ret)
		{
			this.tableModel.setUnchanged();
		}
		
		return ret;
	}
	
	public boolean isChanged() {
		return this.tableModel.isChanged();
	}
	
	public void createVariable() {
		this.tableModel.createVariable();
		boolean ret = this.table.editCellAt(this.tableModel.getRowCount()-1, 0);
		Console.debug(String.format("edit var %d , %d %b", this.tableModel.getRowCount(), 0,ret));
		//this.table.setColumnSelectionInterval(this.tableModel.getColumnCount()-1,this.tableModel.getColumnCount()-1);
	}
	
	public void deleteSelected() {
		int[] rows = this.table.getSelectedRows();
		this.tableModel.deleteRows(rows);
	}



	@Override
	public void keyPressed(KeyEvent e) {
		
		
		int col,row;
		
		switch(e.getKeyCode())
		{
			case KeyEvent.VK_S:
				if((e.getModifiers() & KeyEvent.CTRL_MASK) != 0)
				{
		            Console.debug("Variables editor Ctrl+S");
		            this.save();
		            e.consume();
				}
				
				break;
			case KeyEvent.VK_E:
				if((e.getModifiers() & KeyEvent.CTRL_MASK) != 0)
				{
	       			Console.debug("Variables Editor Ctrl+E");
        			this.createVariable();
        			e.consume();
				}
				break;
			case KeyEvent.VK_DELETE:
					Console.debug("Variables Editor DEL");
	        		this.deleteSelected();
	        		e.consume();
				break;
			case KeyEvent.VK_TAB:
					Console.debug("Variables Editor TAB");
					
					col = this.table.getSelectedColumn();
					row = this.table.getSelectedRow();
	
					if(col <0 || row <0){
						this.selectCell(0, 0);
						
						e.consume();
						return;
					}
					if(col == 0)
					{
						col++;
					}else{
						col = 0;
						if(row < this.table.getRowCount()-1)
						{
							row++;
						}else{
							row = 0;
						}
					}
					this.selectCell(row, col);
	
					e.consume();
				break;
			case KeyEvent.VK_UP:
				col = this.table.getSelectedColumn();
				row = this.table.getSelectedRow();

				if(row <= 0)
				{
					row = this.table.getRowCount()-1;
				}else{
					row--;
				}
				selectCell(row, col);
				e.consume();
				break;
			case KeyEvent.VK_DOWN:
				col = this.table.getSelectedColumn();
				row = this.table.getSelectedRow();
				
				if(row >= this.table.getRowCount()-1)
				{
					row = 0;
				}else{
					row++;
				}
				selectCell(row, col);
				e.consume();
				break;	
			case KeyEvent.VK_LEFT:
			case KeyEvent.VK_RIGHT:
				col = this.table.getSelectedColumn();
				row = this.table.getSelectedRow();
				
				if(col == 0)
				{
					col = 1;
				}else{
					col = 0;
				}
				
				selectCell(row, col);
				e.consume();
				break;
		}

	}

	
	private void selectCell(int row,int col)
	{
		if(this.table.isEditing()){
			  this.table.getCellEditor().stopCellEditing();
		}
		table.getSelectionModel().setSelectionInterval(row, row);
        table.getColumnModel().getSelectionModel().setSelectionInterval(col, col);
	}
	

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}


}
