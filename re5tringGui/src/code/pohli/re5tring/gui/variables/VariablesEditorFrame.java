package code.pohli.re5tring.gui.variables;

import java.awt.Color;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.table.TableColumn;

import code.pohli.re5tring.placeholder.variables.PVarList;
import code.pohli.re5tring.settings.General;
import code.pohli.re5tring.settings.Settings;
import code.pohli.utils.console.Console;




public class VariablesEditorFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -398113161123845062L;
	
	private VariablesEditor editor;
	
	
	public void init() {		
		
		this.setTitle("Variables Editor");
		
		
		
		JMenuBar menubar = new JMenuBar();
		
		JMenu menu = new JMenu("Project");
		
		
		
		
		JMenuItem open = new JMenuItem("Open");
		open.addActionListener(event->{
			this.openFile();
		});
		JMenuItem save = new JMenuItem("Save");
		save.addActionListener(event->{
			this.saveFile();
		});
		JMenuItem close = new JMenuItem("Close");
		close.addActionListener(event->{
			this.closeFile();
		});
		
		menu.add(open);
		menu.addSeparator();
		menu.add(save);
		menu.addSeparator();
		menu.add(close);
		menubar.add(menu);
		
		
		JMenuItem reset = new JMenuItem("Reset");
		reset.addActionListener(event->{
			this.resetEditor();
			
		});
		JMenuItem add = new JMenuItem("Add");
		add.addActionListener(event->{
			this.addVariableToEditor();
		});
		JMenuItem delete = new JMenuItem("Delete");
		delete.addActionListener(event->{
			this.deleteVariableFromEditor();
		});
		
		JMenu edit = new JMenu("Edit");
		
		
		edit.add(reset);
		edit.add(add);
		edit.add(delete);
		
		menubar.add(edit);
		
		this.setJMenuBar(menubar);
		
		
	    
		
		this.editor = new VariablesEditor();
		
		
		this.add(this.editor);
		
		
		
		
		
		this.setSize(600, 400);
		
		this.setVisible(true);
		
		VariablesEditorFrame self = this;
		
		this.addWindowListener(new WindowListener() {
			
			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowClosing(WindowEvent e) {
				Console.debug("Window closing");
				
				self.closeFile();
				
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
	}


	private void closeFile() {
		Console.debug("close File");
		
		if(this.editor.isChanged())
		{
			int ret = JOptionPane.showOptionDialog(this, "Save File bevore closing?", "Close", JOptionPane.YES_NO_CANCEL_OPTION,  JOptionPane.QUESTION_MESSAGE, null, null, null);
			Console.debug(String.valueOf(ret));
			switch(ret)
			{
				case JOptionPane.CANCEL_OPTION:
					break;
				case JOptionPane.OK_OPTION:
					if(this.saveFile()) {
						this.dispose();
						System.exit(0);
					}
					break;
				case JOptionPane.NO_OPTION:
					this.dispose();
					System.exit(0);
					break;
				
			}
			
		}else {
			this.dispose();
			System.exit(0);
		}
		
	}


	private boolean saveFile() {
		Console.debug("save File");
		
		
		if(this.editor.save()) {
			Console.debug("Saved");
			return true;
		}else {
			Console.log("Saving failed");
			return false;
		}
		
	}


	private void openFile() {
		Console.debug("open File");
		Settings s = General.getSettings();
		
		
		
		
		
	}
	
	public void openFile(String path)
	{
		PVarList list = PVarList.load(path);
		
		this.editor.setVarList(list);
	}
	


	private void deleteVariableFromEditor() {
		Console.debug("delete Var");
		this.editor.deleteSelected();
		
	}


	private void addVariableToEditor() {
		Console.debug("add Var");
		this.editor.createVariable();
	}


	private void resetEditor() {
		Console.debug("reset");
		
		this.editor.reset();
		
	}
	

}
