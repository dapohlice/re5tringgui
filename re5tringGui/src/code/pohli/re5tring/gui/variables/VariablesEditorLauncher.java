package code.pohli.re5tring.gui.variables;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.UIManager;

import code.pohli.re5tring.settings.General;
import code.pohli.utils.console.Console;

public class VariablesEditorLauncher {

	public static void main(String[] args) {
		
		 try
		    {
		    	
		    	
		    	
		      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
//		      UIManager.setLookAndFeel( "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel" );
		    }
		    catch ( Exception e )
		    {
		      e.printStackTrace();
		    }
		
		Console.console.setDebug(true); 
		 
		General.initialise();
		
		VariablesEditorFrame editorframe = new VariablesEditorFrame();
		editorframe.init();
		editorframe.openFile(General.getSettings().getWorkingDir());

	}

}
